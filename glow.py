import colorsys
from PyGlow import PyGlow

def float_to_led_int(innum):
    return int(innum * 255)

class GlowHelper:
    def __init__(self):
        self.pyglow = PyGlow()
    def rgb(self, color, white=0, yellow=0, orange=0):
        self.pyglow.color("white", white)
        self.pyglow.color("yellow", yellow)
        self.pyglow.color("orange", orange)
        color = color.lstrip('#')

        lv = len(color)
        rgb = tuple(int(color[i:i + lv // 3], 16) for i in range(0, lv, lv // 3))
        self.pyglow.color("red", rgb[0])
        self.pyglow.color("green", rgb[1])
        self.pyglow.color("blue", rgb[2])

    def hsl(self, hsl):
        (h, s, l) = hsl

        h = h/360
        (r, g, b) = colorsys.hls_to_rgb(h, 0.5, s)

        self.pyglow.color("red", float_to_led_int(r * s))
        self.pyglow.color("green", float_to_led_int(g * s))
        self.pyglow.color("blue", float_to_led_int(b * s))
        self.pyglow.color("white", float_to_led_int(l * (1-s)))

        print "hsl", h,s,l
        print "rgb", r,g,b

