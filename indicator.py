# indicator.py
# Mike Lim <mike@mjlim.net>

from glow import *

glow = GlowHelper()

def active():
    glow.pyglow.all(0)
    glow.pyglow.color("blue", 100)

def listening():
    glow.pyglow.all(0)
    glow.pyglow.color("green", 100)

def inactive():
    glow.pyglow.all(0)
    glow.pyglow.color("red", 100)
