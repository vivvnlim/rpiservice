# controlservice.py
# Mike Lim <mike@mjlim.net>

import os
from btcommandsvc import BtCommandService
from glow import *

commands = {}

glow = GlowHelper()

def cmd_white():
    glow.pyglow.all(255)
def cmd_red():
    glow.pyglow.all(0)
    glow.pyglow.color("red", 255)
    glow.pyglow.color("orange", 200)
    glow.pyglow.color("yellow", 170)
def cmd_off():
    glow.pyglow.all(0)
def cmd_sun():
    glow.rgb("#FAA719", 100, 255, 255)


def cmd_test():
    print "testing"
def cmd_test2():
    print "second test command"
def cmd_shutdown():
    print "shutdown requested"
    os.system("shutdown -h now")
def cmd_listcommands():
    print "\n".join(commands.keys())
    return str(len(commands)) + "\n" + "\n".join(commands.keys()) + "\n"

commands = {'white': cmd_white,
            'red': cmd_red,
            'sun': cmd_sun,
            'off': cmd_off,
            'listcommands': cmd_listcommands,
            'shutdown': cmd_shutdown}
            
btsvc = BtCommandService(commands)
btsvc.listen()
