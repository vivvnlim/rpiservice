# controlservice.py
# Mike Lim <mike@mjlim.net>

import os
import messaging_pb2
import struct
import subprocess
import string
from btcommandsvc import BtCommandService
from glow import *

glow = GlowHelper()

def cmd_get_menu(client_sock, args):
    send_menu(client_sock, menu_dict_to_proto(menus[args[0]]))

def cmd_get_ip(client_sock, args):
    #send_fyi(client_sock, subprocess.check_output(['hostname', '-i']))
    send_bigfyi(client_sock, subprocess.check_output(['ip', 'addr', 'show', 'up']))
def cmd_piglow_rgb(client_sock, args):
    if len(args) >= 4:
        glow.rgb(args[0], int(args[1]), int(args[2]), int(args[3]))
    elif len(args) >= 1:
        glow.rgb(args[0])
    else:
        send_fyi(client_sock, "Not enough args to glow")

def cmd_poweroff(client_sock, args):
    send_fyi(client_sock, "shutdown requested")
    glow.rgb("#000000", 0, 0, 0)
    os.system("shutdown -h now")

def cmd_get_wifi_menu(client_sock, args):
    output = subprocess.check_output(['netctl', 'list'])
    profile_list = [string.strip(x) for x in string.split(output, '\n')][:-1]
    menu = messaging_pb2.MenuItems()
    for profile in profile_list:
        item = menu.items.add()
        item.label = profile
        item.commandAction = 'connect_wifi_profile'
        item.commandArgs.append(profile)
    send_menu(client_sock, menu)

def cmd_connect_wifi_profile(client_sock, args):
    send_fyi(client_sock, "trying " + args[0])
    send_menu(client_sock, menu_dict_to_proto(menus['main']))
    output = subprocess.check_output(['netctl', 'start', args[0]])

def cmd_systemctl(client_sock, args):
    command = [u'systemctl']
    command.extend(args)
    send_fyi(client_sock, "running " + " ".join(command))
    output = subprocess.check_output(command)
    if "" == output:
        send_fyi(client_sock, "ok")
    else:
        send_bigfyi(client_sock, output)

cmd_func_dict = {
        'get_menu': cmd_get_menu,
        'get_ip': cmd_get_ip,
        'piglow_rgb': cmd_piglow_rgb,
        'poweroff': cmd_poweroff,
        'get_wifi_menu': cmd_get_wifi_menu,
        'connect_wifi_profile': cmd_connect_wifi_profile,
        'systemctl': cmd_systemctl
        }

menus = {
        'main': [
        {'label': 'network',
            'commandAction': 'get_menu',
            'commandArgs': ['network']},
        {'label': 'lighting',
            'commandAction': 'get_menu',
            'commandArgs': ['lighting']},
        {'label': 'power',
            'commandAction': 'get_menu',
            'commandArgs': ['power']}
        ],

        'network': [
        {'label': 'IP Address',
            'stayAfter': True,
            'commandAction': 'get_ip'},
        {'label': 'wifi menu',
            'stayAfter': True,
            'commandAction': 'get_wifi_menu'},
        {'label': 'ap up',
            'commandAction': 'systemctl',
            'commandArgs' : ['start', 'wifi']},
        {'label': 'ap down',
            'commandAction': 'systemctl',
            'commandArgs' : ['stop', 'wifi']},
        ],

        'power': [
        {'label': 'poweroff',
            'stayAfter': True,
            'commandAction': 'poweroff',}
        ],

        'lighting': [
        {'label': 'back',
            'stayAfter': True,
            'commandAction': 'get_menu',
            'commandArgs': ['main']},
        {'label': 'red',
            'stayAfter': True,
            'commandAction': 'piglow_rgb',
            'commandArgs' : ['#FF0000', '0', '170', '200']},
        {'label': 'the sun',
            'stayAfter': True,
            'commandAction': 'piglow_rgb',
            'commandArgs' : ['#FAA719', '100', '255', '255']},
        {'label': 'off',
            'stayAfter': True,
            'commandAction': 'piglow_rgb',
            'commandArgs' : ['#000000', '0', '0', '0']},
        ]
    }

def handle_connection(client_sock):
    while True:
        message = read_message(client_sock)
        if message == None: break
        print("received [%s]" % message)
        if message.HasField('command'):
            # this message has a command so process it
            dispatch_command(client_sock, message.command)
            #if message.command.action == "echo":
                #print("responding to echo")
                #send_menu(client_sock, menu_dict_to_proto(mainMenu))
                #send_command(client_sock, "testCommand")

def read_message(client_sock):
    # read length, 32-bit integer
    lengthdata = recv_num(client_sock, 4)
    if len(lengthdata) != 4:
        print("received length was not 4, was " + str(len(lengthdata)))
    length = struct.unpack("!i", lengthdata)[0]
    print("new message of length " + str(length))
    data = recv_num(client_sock, length)
    if len(data) > 0:
        message = messaging_pb2.Message()
        message.ParseFromString(data)
        return message
    else:
        return None

def recv_num(client_sock, length):
    data = ""
    while len(data) < length:
        buf = client_sock.recv(length - len(data))
        if not len(buf):
            break
        data += buf
    return data

def send_message(client_sock, message):
    data = message.SerializeToString()
    client_sock.send(struct.pack("!i", len(data)))
    client_sock.send(data)

def send_command(client_sock, action, args=None):
    message = messaging_pb2.Message()
    message.command.action = action
    if args != None:
        message.command.args.extend(args)

    send_message(client_sock, message)

def send_menu(client_sock, menu):
    print("Sending menu " + str(menu))
    message = messaging_pb2.Message()
    message.menuItems.CopyFrom(menu)
    send_message(client_sock, message)

def send_fyi(client_sock, message):
    send_command(client_sock, 'fyi', [message])
def send_bigfyi(client_sock, message):
    send_command(client_sock, 'bigfyi', [message])


def menu_dict_to_proto(menu_desc):
    menu = messaging_pb2.MenuItems()
    for item_desc in menu_desc:
        item = menu.items.add()
        if 'label' in item_desc:
            item.label = item_desc['label']
        if 'stayAfter' in item_desc:
            item.stayAfter = item_desc['stayAfter']
        if 'commandAction' in item_desc:
            item.commandAction = item_desc['commandAction']
        if 'commandArgs' in item_desc:
            item.commandArgs.extend(item_desc['commandArgs'])

    return menu

def dispatch_command(client_sock, command):
    try:
        if command.action in cmd_func_dict:
            cmd_func_dict[command.action](client_sock, command.args)
    except Exception as e:
        print("Exception while dispatching command: " + str(e))
        send_bigfyi(client_sock, str(e))


btsvc = BtCommandService(handle_connection)
btsvc.poke_client() # tell watch to start the app
btsvc.listen()
