from flask import Flask
from glow import *

app = Flask(__name__)
glow = GlowHelper()

@app.route("/")
def hello():
    return "hello world"

@app.route("/rgb/<rgbcolor>")
def show_rgb_color(rgbcolor):
    glow.rgb(rgbcolor, 0, 0, 0)
    return "ok"

@app.route("/hsl/<h>/<s>/<l>")
def show_hsl_color(h,s,l):
    glow.hsl((float(h),float(s),float(l)))
    return "ok"


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80, debug=True)
