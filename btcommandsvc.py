# btcommandsvc.py
# Mike Lim <mike@mjlim.net>
#
# based on /usr/share/doc/python-bluez/examples/simple/rfcomm-server.py 
# by Albert Huang <albert@csail.mit.edu>

import indicator
import threading

from bluetooth import *

class BtCommandService:
    def __init__(self, connection_handler):
        indicator.inactive()
        self.connection_handler = connection_handler
        self.server_sock=BluetoothSocket( RFCOMM )
        self.server_sock.bind(("",15))
        self.server_sock.listen(5)

        self.port = self.server_sock.getsockname()[1]

        self.uuid = "94f39d29-7d6d-437d-973b-fba39e49d4ee"
        #self.uuid = "787d84c0-bfe7-11e4-a04d-0002a5d5c51b"

        advertise_service( self.server_sock, "mjl-rpi2 control service",
                        service_id = self.uuid,
                        service_classes = [ self.uuid, SERIAL_PORT_CLASS ],
                        profiles = [ SERIAL_PORT_PROFILE ],  # if creating SERIAL_PORT_PROFILE fails on archlinux, try adding --compat to bluetoothd startup options
        #                   protocols = [ OBEX_UUID ] 
                            )
                   
    def listen(self):
        while True:
            print("Waiting for connection on RFCOMM channel %d" % self.port)
            indicator.listening()

            try:
                client_sock, client_info = self.server_sock.accept()
                print("Accepted connection from ", client_info)
                indicator.active()
                self.connection_handler(client_sock)
            except IOError:
                indicator.inactive()
                print("IOError")
                if 'client_sock' in locals():
                    client_sock.close()
                pass
            except KeyboardInterrupt:
                indicator.inactive()
                print("terminating")
                break

            print("disconnected")

        if 'client_sock' in locals():
            client_sock.close()
        self.server_sock.close()
        print("all done")

    def poke_client(self):
        # kicks off poke and returns
        t = threading.Thread(target=self.do_poke_client)
        t.start()
    def do_poke_client(self):
        # poke client to try to connect
        try:
            poke_socket = BluetoothSocket(RFCOMM)
            poke_socket.connect(('2C:54:CF:71:DD:E7', 16)) # connect to my G Watch
            poke_socket.close()
        except:
            pass # expected that the client may refuse the connection. don't care because the intent fires and starts the app
